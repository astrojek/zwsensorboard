#ifndef ZWSENSORBOARD_ZWCONFIG_H
#define ZWSENSORBOARD_ZWCONFIG_H


class zwConfig {
private:
    String _api_key;
    bool _modified;

    zwConfig();

    ~zwConfig();

public:
    bool load();

    bool save();

    const String &api_key();

    void set_api_key(const String &key);

    void mark_modified();

    static zwConfig &instance() {
        static zwConfig config;
        return config;
    }
};


#endif //ZWSENSORBOARD_ZWCONFIG_H
