#include <WiFiManager.h>

#include "zwConfig.h"
#include "zwWifi.h"


zwWifi::zwWifi() {
    this->_custom_api_key = new WiFiManagerParameter("api_key", "API Key", zwConfig::instance().api_key().c_str(), 20);
    this->_wifi_manager = new WiFiManager();

    this->_wifi_manager->setSaveConfigCallback([]() { zwConfig::instance().mark_modified(); });

    this->_wifi_manager->addParameter(this->_custom_api_key);
}


zwWifi::~zwWifi() {
}

bool zwWifi::need_internet() {
    bool result = this->_wifi_manager->autoConnect("zwSensorAP");

    zwConfig::instance().set_api_key(this->_custom_api_key->getValue());

    return result;
}
