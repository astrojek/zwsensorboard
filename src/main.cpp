#include <FS.h>

#include <Arduino.h>

#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic

#include <ESP8266HTTPClient.h>

#include <DHT.h>

#include "zwConfig.h"
#include "zwWifi.h"

// DHT Section
#define DHTPIN    D4
#define DHTTYPE   DHT22

DHT dht(DHTPIN, DHTTYPE);

const int sleepSeconds = 5*60;

void save_result(float temp, float humidity, float heat_index);

void setup() {
    // initialize serial communications at 9600 bps for debug purpose
    Serial.begin(9600);
    Serial.println("\n\nWake up");

    // Start from loading configuration
    zwConfig::instance().load();

    // Debug stuff, print what has been read
    Serial.print("Read API Key: ");
    Serial.print(zwConfig::instance().api_key());
    Serial.println();

    // Connect D0 to RST to wake up
    // pinMode(D0, WAKEUP_PULLUP);

    if (!zwWifi::instance().need_internet()) {
        Serial.println("failed to connect and hit timeout");
        delay(3000);
        // Reset and try again, or maybe put it to deep sleep
        ESP.reset();
        delay(5000);
    }

    zwConfig::instance().save();

    dht.begin();

}

void loop() {
    delay(2000);
    float h = dht.readHumidity();
    float t = dht.readTemperature();

//     Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
        Serial.println("Failed to read from DHT sensor!");
        return;
    }

    float hi = dht.computeHeatIndex(t, h, false);

    save_result(t, h, hi);

//     convert to microseconds
    Serial.printf("Deep sleep for %d seconds\n\n", sleepSeconds);
//    ESP.deepSleep(sleepSeconds * 1000000);
    delay(sleepSeconds * 1000);
}

void save_result(float temp, float humidity, float heat_index) {
    HTTPClient http;
    http.begin("https://api.thingspeak.com/update"); //HTTP

    String payload = "api_key=";
    payload += zwConfig::instance().api_key();
    payload += "&field1=";
    payload += temp;
    payload += "&field2=";
    payload += humidity;
    payload += "&field3=";
    payload += heat_index;

    Serial.print("Payload: ");
    Serial.println(payload);

    int status_code = http.POST(payload);
    Serial.printf("Status code: %d\n", status_code);
    http.end();
}
