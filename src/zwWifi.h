#ifndef ZWSENSORBOARD_ZWWIFI_H
#define ZWSENSORBOARD_ZWWIFI_H


class WiFiManager;

class WiFiManagerParameter;


class zwWifi {
private:
    zwWifi();

    ~zwWifi();

    WiFiManagerParameter *_custom_api_key;
    WiFiManager *_wifi_manager;

public:
    bool need_internet();
    
    static zwWifi &instance() {
        static zwWifi wifi;
        return wifi;
    }
};


#endif //ZWSENSORBOARD_ZWWIFI_H
