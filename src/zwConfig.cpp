#include <FS.h>
#include <ArduinoJson.h>

#include "zwConfig.h"


zwConfig::zwConfig() : _modified(false), _api_key("") {
}

zwConfig::~zwConfig() {
}

bool zwConfig::load() {
    // Start
    if (SPIFFS.begin()) {
        if (SPIFFS.exists("/config.json")) {
            // File exists, reading and loading
            File configFile = SPIFFS.open("/config.json", "r");

            if (configFile) {
                size_t size = configFile.size();
                // Allocate a buffer to store contents of the file.
                std::unique_ptr<char[]> buf(new char[size]);

                configFile.readBytes(buf.get(), size);
                DynamicJsonBuffer jsonBuffer;
                JsonObject& json = jsonBuffer.parseObject(buf.get());
                if (json.success()) {
                    this->_api_key = json["api_key"].as<String>();
                    return true;
                }
            }
        }
    }

    return false;
}

bool zwConfig::save() {
    //save the custom parameters to FS
    if (this->_modified) {
        Serial.println("saving config");
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.createObject();
        json["api_key"] = this->_api_key;

        File configFile = SPIFFS.open("/config.json", "w");
        if (!configFile) {
            Serial.println("failed to open config file for writing");
        }

        json.printTo(Serial);
        json.printTo(configFile);
        configFile.close();
        //end save
    }
}

const String& zwConfig::api_key() {
    return this->_api_key;
}


void zwConfig::set_api_key(const String &key) {
    this->_api_key = key;
}


void zwConfig::mark_modified() {
    this->_modified = true;
}